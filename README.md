# Mycroft for Hass.io

[Mycroft](https://mycroft.ai) is an open source voice assistant. [Hass.io](https://www.home-assistant.io/hassio/) turns your Raspberry Pi (or another device) into the ultimate home automation hub powered by Home Assistant. This addon integrates Mycroft into Hass.io without the need for a second device.

## Installation

1. Visit your [local Hass.io dashboard](http://hassio:8123/hassio) and select the Addons tab.
2. Add _https://gitlab.com/ndarilek/hassio-mycroft_ to your list of addon repositories.
3. Click the _PulseAudio_ addon, then click _Install_. See the below section about PulseAudio if you're considering skipping this step.
4. Click the _Mycroft_ addon, then click _Install_.
5. Click _Start_, and wait a few minutes for the initial skill installation.

## Is PulseAudio necessary?

[In theory, no.](https://github.com/MycroftAI/mycroft-core/issues/1676) However, I only briefly got this working, and to this day I'm not sure how or what changed. The internet is [full](https://stackoverflow.com/questions/43947294/i-have-trouble-with-pyaudio-does-not-detect-input-device) [of](https://raspberrypi.stackexchange.com/questions/59852/pyaudio-does-not-detect-my-microphone-connected-via-usb-audio-adapter) people not having input devices recognized by PyAudio, and indeed the code in the previously-linked issue completely fails to detect my USB input despite it working for paplay/parecord/aplay/arecord. So for the moment, PulseAudio is the default configuration.

I'd accept patches to make this work with only ALSA though. If you'd like to remove PulseAudio in your own setup, simply set the _pulse_server_ configuration option to an empty string and remove _/config/mycroft/.asoundrc_. You can also modify _/config/mycroft/.asoundrc_ to add your own settings, and it won't be overwritten on addon restart. Remove it to regenerate the default, or replace it with an empty file to avoid adding extra configuration.

## Using Mycroft

[Mycroft's own documentation](https://mycroft.ai/documentation/) is quite good and should be consulted for most things. This section only concerns Mycroft use as it relates to this addon.

### File Locations

Everything is located in _/config/mycroft_. Here are some locations of note:

 * _/config/mycroft/.mycroft_: This is equivalent to _$HOME/.mycroft_. Here you'll find your user-specific _mycroft.conf_, as well as any data that Mycroft needs to download or cache.
 * _/config/mycroft/skills_: Mycroft's skills reside here. See the below notes on how to install skills.
 * _/config/mycroft/venv_: This is Mycroft's main virtual environment. See the below notes on managing this.

### Installing Skills

If a skill doesn't require any native dependencies, you should be able to SSH into your instance, install Python, optionally install _msm_ via Pip, activate the virtualenv at _/config/mycroft/venv_, and simply run:

```bash
$ msm install <skill>
```

You can also clone git repositories directly, or can push local repositories to remotes under _/config/mycroft/skills_ and things usually work. Skill repositories are updated by default every hour. It may be necessary to restart Mycroft if dependencies change or if something gets extremely broken.

Things get a bit trickier if your skill has native dependencies, because Hass.io's containers are based on Alpine/Musl while Mycroft uses Raspbian and the official packages. In the case of native dependencies, msm will have to run in the context of the container itself.

The easiest way to do this is to log into [home.mycroft.ai](https://home.mycroft.ai), configure the custom installer skill, and set the URL to your skill's git repository. Then say "download custom skill" and Mycroft will download the skill into the container instance, installing native requirements compiled against the correct native libraries.

### Managing the Virtualenv

Since Mycroft skills require their own dependencies, and since it is impossible to know in advance which skills a given instance will need, Mycroft manages its own virtualenv. I imagine this virtualenv might get out-of-sync at times, or that major Python API bumps might break native dependencies.

If that happens, you may need to `rm -rf /config/mycroft/venv /config/mycroft/skills` and then reinstall any non-default skills. It is necessary to remove skills because I think msm only installs requirements when skills are initially installed. If it detects that the skill already exists, it won't re-install dependencies and the skill will break.
